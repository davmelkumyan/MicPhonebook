﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Mic.PhoneBook
{
    class PhoneBook
    {
        public int Count { get => contacts.Count; }
        private List<Contact> contacts;

        //indexer
        public Contact this[int index]
        {
            get => contacts[index];
            set => contacts[index] = value;
        }

        public void AddContact(string name, string surName, string number, string email)
        {
            if (contacts == null)
                contacts = new List<Contact>();

            Contact contact = new Contact(name, surName, number, email);
            Checker(number, contact);
        }

        private void Checker(string number, Contact contact)
        {
            bool areEqual = true;
            int staticCount = contacts.Count;
            if (contacts.Count > 0)
                for (int i = 0; i < staticCount; i++)
                {
                    if (contacts[i].Number != number)
                        areEqual = false;
                    else
                    {
                        areEqual = true;
                        break;
                    }
                }
            else
                contacts.Add(contact);

            if (!areEqual)
                contacts.Add(contact);
        }

        public void DeleteContact(int index)
        {
            if (contacts != null)
                contacts.RemoveAt(index);
        }

        public void ClearContactList()
        {
            for (int i = 0; i < Count; i++)
            {
                contacts.RemoveRange(0, Count);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Mic.PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            PhoneBook phoneBook = new PhoneBook();
            Imitate.Contact(phoneBook);
            string code = "055";
            string[] contactList = new Contact().GetCurrentCode(phoneBook, code);
            //for (int i = 0; i < contactList.Length; i++)
            //{
            //    Console.WriteLine(contactList[i] + $"\n{new string('-', 20)}");
            //}

            phoneBook.ClearContactList();
            Console.WriteLine($"\nContact count with code: '{code}' \n: {contactList.Length} contacts");
            //delay
            Console.ReadKey();
        }
    }
}

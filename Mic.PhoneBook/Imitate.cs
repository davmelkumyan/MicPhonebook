﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.PhoneBook
{
    static class Imitate
    {
        public static void Contact(PhoneBook phoneBook)
        {
            Random rnd = new Random();
            string[] codes = { "099", "077", "093", "055", "098", "091", "041" };
            string[] mailscods = { "@mail.ru", "@my.com", "@yandex.ru", "@gmail.com", "@yahoo.com", "@index.ru", "@bk.ru" };
            for (int i = 0; i < 20000; i++)
            {
                string name = $"Abc{i + 1}";
                string surName = $"Abc{i + 1}yan";
                string number = $"{codes[rnd.Next(0, codes.Length)]}{rnd.Next(100, 999)}{rnd.Next(100, 999)}";
                string email = $"{name}.{surName}{mailscods[rnd.Next(0, mailscods.Length)]}";
                phoneBook.AddContact(name, surName, number, email);
            }
        }
    }
}

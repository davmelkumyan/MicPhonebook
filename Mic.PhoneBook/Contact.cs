﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;

namespace Mic.PhoneBook
{
    class Contact : IFormattable
    {
        public string Code { get; private set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }

        public Contact()
        {

        }

        public Contact(string name, string surName, string number, string email)
        {
            Name = name;
            SurName = surName;
            if (number.Length >= 9)
                Number = number;
            if (email.EndsWith(".ru") || email.EndsWith(".com"))
                Email = email;
            else
                throw new Exception("Wrong format for Email");
            if (this.Number.Length > 9)
                Code = "0" + Number.Substring(3, 2);
            else if (this.Number.Length == 9)
                Code = Number.Substring(0, 3);
            else
                throw new Exception("Number is less then '9' ");
        }

        public string[] GetCurrentCode(PhoneBook pb, string code)
        {
            List<string> numList = new List<string>();
            for (int i = 0; i < pb.Count; i++)
            {
                if (pb[i].Code.SequenceEqual(code))
                    numList.Add(pb[i].ToString());
            }
            return numList.ToArray();
        }

        //To Strings
        public override string ToString()
        {
            return ToString(Standart.N, CultureInfo.CurrentCulture);
        }

        public string ToString(string format)
        {
            return ToString(format, CultureInfo.CurrentCulture);
        }

        public string ToString(IFormatProvider provider)
        {
            return ToString(Standart.N, provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            if (string.IsNullOrEmpty(format))
                format = Standart.N;

            if (provider == null)
                provider = CultureInfo.CurrentCulture;

            var builder = new StringBuilder();

            switch (format.ToUpperInvariant())
            {
                case Standart.N:
                    builder.AppendFormat("  ** Contact **\nName : {0}\nSurName : {1}\nNumber : {2}\nEmail : {3}", Name, SurName, Number, Email);
                    break;
                case Standart.A:
                    builder.AppendFormat("  || Contact || Name | {0} SurName | {1} Number | {2} Email | {3}", Name, SurName, Number, Email);
                    break;
                case Standart.B:
                    builder.Append(new string('-', 15));
                    builder.Append("\n___ Contact ___\n");
                    builder.Append(new string('-', 15));
                    builder.AppendFormat("\nName : {0}\nSurName : {1}\nNumber : {2}\nEmail : {3}", Name, SurName, Number, Email);
                    break;
                case Standart.C:
                    builder.AppendFormat("  ** Contact **\nName : {0}\nSurName : {1}\nNumber : {2}\nEmail : {3}", Name, SurName, Number, Email);
                    builder.Append($"\n{DateTime.Now:dd:MM:yyyy}");
                    break;
                default:
                    return "No Format changed";
            }
            return builder.ToString();
        }

    }
}
